from django.test import TestCase
from django.urls import reverse
from .models import TextData

class TextDataViewTestCase(TestCase):

    def setUp(self):
        TextData.objects.create(text="Sample Text 1")
        TextData.objects.create(text="Sample Text 2")

    def test_text_data_view(self):
        url = reverse('text_data')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainpage.html')
        self.assertIn('text_data', response.context)
        self.assertEqual(len(response.context['text_data']), 2)

