from django.shortcuts import render
from .models import TextData

def text_data_view(request):
    
    text_data = TextData.objects.all()
    return render(request, 'mainpage.html', {'text_data': text_data})
